package main

import (
	"flag"
	"log"
	"net"
	"net/http"
	"os"

	"github.com/gorilla/websocket"
)

var addr = flag.String("addr", "localhost:8080", "http service address")

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// Websocket -> Database
func inbound(conWebsocket *websocket.Conn, conDB net.Conn, stop chan bool) {
	for {
		_, message, err := conWebsocket.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			stop <- true
		}

		log.Println("Websocket > Database", message)

		conDB.Write(message)
	}
}

// Database -> Websocket
func outbound(conWebsocket *websocket.Conn, conDB net.Conn, stop chan bool) {
	buffer := make([]byte, 1024)

	for {
		n, err := conDB.Read(buffer)
		message := buffer[:n]

		if err != nil {
			log.Println("Write to server failed:", err.Error())
			stop <- true
		}

		log.Println("Websocket < Database", message)

		conWebsocket.WriteMessage(2, message)
	}
}

func echo(w http.ResponseWriter, r *http.Request) {
	stop := make(chan bool)

	conWebsocket, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("upgrade:", err)
		return
	}

	defer conWebsocket.Close()

	servAddr := "amzdash.cb7zb7rrjplx.us-east-1.rds.amazonaws.com:3306"
	tcpAddr, err := net.ResolveTCPAddr("tcp", servAddr)
	if err != nil {
		log.Println("ResolveTCPAddr failed:", err.Error())
		stop <- true
	}

	conDB, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		log.Println("Dial failed:", err.Error())
		stop <- true
	}

	log.Println("New connection", conDB)

	go outbound(conWebsocket, conDB, stop)
	go inbound(conWebsocket, conDB, stop)

	select {
	case <-stop:
		log.Println("Websocket connection closed")
		return
	}
}

func main() {
	flag.Parse()
	log.SetFlags(0)
	http.HandleFunc("/echo", echo)
	log.SetOutput(os.Stdout)
	log.Fatal(http.ListenAndServe(*addr, nil))
}
